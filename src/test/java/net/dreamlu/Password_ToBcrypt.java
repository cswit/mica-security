/*
 * Copyright (c) 2019-2029, Dreamlu 卢春梦 (596392912@qq.com & www.dreamlu.net).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dreamlu;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Scanner;

public class Password_ToBcrypt {
	public static void main(String[] args) throws NoSuchAlgorithmException {
		Scanner sc;
		String password;
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		while (true) {
			System.out.println("please input new passwd or (exit/quit/q): ");
			sc = new Scanner(System.in);
			if (sc.hasNextLine()) {
				password = sc.nextLine();
				if (password.equals("exit")) break;
				else if (password.equals("quit")) break;
				else if (password.equals("q")) break;
				System.out.println("password:	" + encoder.encode(password));
			}
		}
	}
}
